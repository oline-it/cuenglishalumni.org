
    <!-- Page Content -->
    <div class="container upcomingevent">
		<div class="row">
			<div class="col-md-6 left">
				<div class="upoverlay text-center">

				</div>
				<img src="../resource/img/bg-3.jpg" alt="upcoming" class="img img-responsive"/>
			</div>
			<div class="col-md-6 right">
				<div class="area">
					<center><a href="#" class="btn btn-default upcomingbtn">Upcoming Event</a></center>
					<div id="countdown" class="table-responsive"></div>
					<h4>We are going to arrange a Reunion !</h4>
					<p class="text-justify">We welcome you all to the new website of The Chittagong University English Alumni Association. From now on, you will be regularly updated about the activities of the association and the English Department here. If you have received any degree, such as BA (Honours), MA, MPhil, or PhD from this department, you are one of our valued and proud alumni. We want this site to become a vibrant platform for the alumni across the globe to share their success stories which would inspire our existing students, and guide them to choose their professional pathways. Let’s grow together to serve the nation better.</p>
					<a href="#membership" class="btn btn-default joinbtn">Join With Us</a>
				</div>
			</div>
		</div>
    </div>
	<div class="ourmission" style="background-image: url('../resource/img/mission_bg.png');">
		<div class="container">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
				<div class="misarea">
					<h4>Our Mission</h4>
					<p class="text-justify">
						<span class="firstcharacter">T</span>o grow and strengthen bonds between our Alumni and the Department of English by providing different tangible benefits including career services, networking opportunities, special events and lectures, and the opportunity to connect with and inspire students and graduates. And, to engage Alumni with Departmental research and future goals, in order that each individual feels welcome and valued as a member of the English Literature community.
					</p>
					<a href="#" class="btn btn-default ourmsnbtn">Know More</a>
				</div>
			</div>
			</div>
		</div>
	</div>
	<div class="responsibility">
		<div class="container">
			<h4 class="text-center center-line">Our Responsibility</h4>
			<div class="row">
				<div class="col-md-3">
					<div class="resarea">
						<img src="../resource/img/s1.png" alt="icon1" class="img img-responsive"/>
						<h4>Scholarship</h4>
						<p></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="resarea">
						<img src="../resource/img/s2.png" alt="icon1" class="img img-responsive"/>
						<h4>Help Current Students</h4>
						<p></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="resarea">
						<img src="../resource/img/s3.png" alt="icon1" class="img img-responsive"/>
						<h4>Help Our University</h4>
						<p></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="resarea">
						<img src="../resource/img/s4.png" alt="icon1" class="img img-responsive"/>
						<h4>Build Our Community</h4>
						<p></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="members">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-6 micon"><img src="../resource/img/m1.png" class="img img-responsive"/></div>
						<div class="col-md-6 mtext">
							<p class="counter" data-count="2343">0</p>
							<h5>Member</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-6 micon"><img src="../resource/img/m2.png" class="img img-responsive"/></div>
						<div class="col-md-6 mtext">
							<p class="counter" data-count="2343">0</p>
							<h5>Photos</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-6 micon"><img src="../resource/img/m3.png" class="img img-responsive"/></div>
						<div class="col-md-6 mtext">
							<p class="counter" data-count="05">0</p>
							<h5>Meetings</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-6 micon"><img src="../resource/img/m5.png" class="img img-responsive"/></div>
						<div class="col-md-6 mtext">
							<p class="counter" data-count="04">0</p>
							<h5>Reunion</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--<div class="job">
		<div class="container text-center">
			<h4 class="toph">Recent Job Opportunity</h4>
			<div class="row">
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j1.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnA">Apply Now</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j2.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnB">Apply Now</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j1.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnC">Expired</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j2.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnA">Apply Now</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j1.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnA">Apply Now</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j2.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnB">Apply Now</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j1.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnC">Expired</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="jobarea">
						<img src="resource/img/j2.png" class="img img-responsive" alt="image"/>
						<h4>Accountant</h4>
						<p>YPSA (Young Power in Social Action) www.ypsa.org is an organization for </p>
						<a href="#" class="btn btn-default jobbtnA">Apply Now</a>
					</div>
				</div>
			</div>
			<br/><a href="#" class="btn btn-default joblistbtn">All Job List</a><br/>
		</div>
	</div>-->
	<div class="Gallery">
		<div class="container text-center">
			<h4 class="toph">Gallery</h4>
			<!--<div align="center" id="garea">
				<ul>
					<li class="active"><a class="btn btn-default filter-button current" data-filter="all">All</a></li>
					<li><a class="btn btn-default filter-button" data-filter="hdpe">Old Memories</a></li>
					<li><a class="btn btn-default filter-button" data-filter="sprinkle">Event</a></li>
					<li><a class="btn btn-default filter-button" data-filter="spray">Our Picnic</a></li>
					<li><a class="btn btn-default filter-button" data-filter="irrigation">Recent</a></li>
				</ul>
			</div>-->
			<div align="center">
				<button class="btn btn-default filter-button current" data-filter="all">All</button>
				<button class="btn btn-default filter-button" data-filter="hdpe">Old Memories</button>
				<button class="btn btn-default filter-button" data-filter="sprinkle">Event</button>
				<button class="btn btn-default filter-button" data-filter="spray">Our Picnic</button>
				<button class="btn btn-default filter-button" data-filter="irrigation">recent</button>
			</div>
			
			<br/>
			<div class="row">
				<div class="gallery_product col-md-4 filter hdpe">
					<a href="../resource/img/gallery/a.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/a.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-4 filter sprinkle">
					<a href="../resource/img/gallery/b.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/b.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-4 filter hdpe">
					<a href="../resource/img/gallery/c.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/c.jpg" class="img-responsive">
					</a>
				</div>
			</div>
			<div class="row">
				<div class="gallery_product col-md-6 filter irrigation">
					<a href="../resource/img/gallery/d.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/d.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-6 filter spray">
					<a href="../resource/img/gallery/e.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/e.jpg" class="img-responsive">
					</a>
				</div>

			</div>
			<div class="row">
				<div class="gallery_product col-md-12 filter hdpe">
					<a href="../resource/img/gallery/f.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/f.jpg" class="img-responsive">
					</a>
				</div>

			</div>
		</div>
	</div>
	<div class="scholarshipsection" style="background-image: url('../resource/img/scholarship-bg.png')">
		<div class="container">
			<div class="conarea text-center">
				<h4>Scholarship For Telented Student!</h4>
				<p></p>
				<a href="#" class="btn btn-default schbtn">Apply Now</a> 
			</div>
		</div>
	</div>
	<!--<div class="blog">
		<div class="container">
			<h4 class="text-center">Recent Blog Post</h4>
			<div class="row">
				<div class="col-md-4">
					<div class="blogarea">
						<img src="resource/img/b1.png" alt="blog image" class="img img-responsive"/>
						<div class="text">
							<h5>Recently we creat a maassive project that will be a...</h5>
							<p>Recently we creat a maassive project thatwill be Recently we creat a maassive project that will be Recently maassive </p>
							<a href="#" class="btn btn-default readmorebtn">More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blogarea">
						<img src="resource/img/b1.png" alt="blog image" class="img img-responsive"/>
						<div class="text">
							<h5>Recently we creat a maassive project that will be a...</h5>
							<p>Recently we creat a maassive project thatwill be Recently we creat a maassive project that will be Recently maassive </p>
							<a href="#" class="btn btn-default readmorebtn">More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blogarea">
						<img src="resource/img/b1.png" alt="blog image" class="img img-responsive"/>
						<div class="text">
							<h5>Recently we creat a maassive project that will be a...</h5>
							<p>Recently we creat a maassive project thatwill be Recently we creat a maassive project that will be Recently maassive </p>
							<a href="#" class="btn btn-default readmorebtn">More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
    
