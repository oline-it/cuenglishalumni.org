<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();

$obj->setData($_SESSION);
if(isset($_GET)){
	$obj->setData($_GET);
	if($_GET['downloadcard']=='yes'){
		$auth= new Auth();
		$status= $auth->setData($_GET)->is_registered();
	}
}


$User = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
$objAlumni = new \App\Alumni\Alumni();
$singleUser=$objAlumni->objectToArray($User);
//var_dump($singleUser);
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>.::Chittagong University English Alumni::.</title>
	<!--fonawesome-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--lightbox-->
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.css'>
		<!--Roboto font-->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="../resource/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../resource/css/modern-business.css" rel="stylesheet">
    <link href="../resource/css/style.css" rel="stylesheet">
	
	<style>
	@import url('https://fonts.googleapis.com/css?family=Roboto');
	</style>
		  <!-------- Oline Style --->
	  <link href="../resource/css/animate.min.css" rel="stylesheet">
	  <link href="../resource/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="../resource/bootstrap/css/jquery-ui.css">
	  <script src="../resource/bootstrap/js/jquery.js"></script>
	  <script src="../resource/bootstrap/js/jquery-ui.js"></script>
	  <script src="../resource/js/registration.js"></script>
	  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700" rel="stylesheet">

	  <!-------- Oline Style --->
	  <!-------- Date picker started --->
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	  <script>
		  $( function() {
			  $( "#datepicker" ).datepicker({
				  dateFormat: 'yy-mm-dd',
				  changeMonth: true,
				  changeYear: true,
				  yearRange: '-100y:c+nn',
				  maxDate: '-1d'
			  });
		  } );
	  </script>
	  <!-------- Date picker ended --->
  </head>
  <body>
  <!-- Load Facebook SDK for JavaScript started -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
		  fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your customer chat code -->
  <div class="fb-customerchat"
	   attribution=setup_tool
	   page_id="2548658381839264"
	   theme_color="#002A5C">
  </div>
  <!-- Load Facebook SDK for JavaScript ended -->
	
  <!--Add fb page in meeting page-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
  <!--//-->
	<div class="headertop">
		<div class="container">
			<div style="height:auto;" class="area">
				<div class="row">
					<div class="col-md-6 left text-left">
						<div class="leftarea">
							<a href="#">Email: alumnisouvenir2019@gmail.com</a>
							<a href="#">Hotline: 01714130077</a>
						</div>
					</div>
					<?php
					if(!$status) {
						echo "<div class=\"col-md-6 right text-right\">
						<a href=\"login.php\" class=\"btn btn-default login\">Log In</a>
						<a href=\"registration.php\" class=\"btn btn-default signup\">Sign Up</a>
					</div>";
					} else{
						echo "<div class=\"col-md-6 right text-right\">
						<a href=\"login.php\" class=\"btn btn-default login\">H! $User->full_name</a>
						<a href=\"User/Authentication/logout.php\" class=\"btn btn-default signup\">Log Out</a>
					</div>";
					}

					?>

				</div>
			</div>
		</div>
	</div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark">
      <div class="container">
        <a class="navbar-brand" href="home.php">
			<img src="../resource/img/logo.png" alt="logo" class="img img-responsive"/>
		</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="home.php">Home</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="chairman_message.php">Message From Chairman</a></li>
					<li><a href="committee.php">Alumni Committee</a></li>
					<li><a href="about.php">About Us</a></li>
				</ul>
            </li>
			<li class="nav-item dropdown" >
              <a id="membership" class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Membership <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="registration.php">Online Registration</a></li>
					<li><a href="life_registration.php">Life Member Registration Online</a></li>
					<li><a href="../resource/form/Reg_Form.pdf">Download Registration Form</a></li>
					<li><a href="../resource/form/Life_Reg_Form.pdf">Download Life Membership Registration Form</a></li>
					<li><a href="#">Members</a></li>
				</ul>
            </li>
			
			<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News & Updates<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="events.php">Events</a></li>
					<li><a href="meeting.php">Meeting</a></li>
				</ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gallery.php">Gallery</a>
            </li>
            <!--<li class="nav-item">
              <a class="nav-link" href="index.html">Project</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="blog.php">Blog</a>
            </li>-->
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
			  <?php if($status){
				 $user=" <li class=\"nav-item dropdown\">
					  <a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Account<span class=\"caret\"></span></a>
					  <ul class=\"dropdown-menu\">
						  <li><a href=\"profile.php\">Profile</a></li>
						  <li><a href=\"profile.php?dpdf=yes\">Download Invitation Card</a></li>
					  </ul>
				  </li>";
			  $admin=" <li class=\"nav-item dropdown\">
					  <a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Account<span class=\"caret\"></span></a>
					  <ul class=\"dropdown-menu\">
						  <li><a href=\"registered_member.php\">Dashboard</a></li>
						  <li><a href=\"profile.php\">Profile</a></li>
						  <li><a href=\"profile.php?dpdf=yes\">Download Invitation Card</a></li>
					  </ul>
				  </li>";
			  if($singleUser['role']=='Admin'){
				  echo $admin;
			  }else{
				  echo $user;
			  }
			  }

			  ?>
            
          </ul>
        </div>
      </div>
    </nav>
  <?php echo "<div style='height: auto; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?>
	<div class="container">
		<div class="scrl"><!--scrollup-->
			<marquee scrollamount="5"  onMouseOver="this.stop()" onMouseOut="this.start()">
			<img width="30px" src="../resource/img/new.gif"><a class="scrolla" href="registration.php">Register for 4th Alumni Reunion-2019</a> Or <a class="scrolla" href="../resource/form/Reg_Form.pdf">Download Registration Form</a> &nbsp;/&nbsp;<a class="scrolla" href="../resource/form/Life_Reg_Form.pdf"> Life Membership Registration Form</a>
          </marquee>
		</div>
	</div>
