<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

   <title>.::Chittagong University English Alumni::.</title>
	<!--fonawesome-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--lightbox-->
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.css'>
	
	<!--Roboto font-->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="../resource/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../resource/css/modern-business.css" rel="stylesheet">
    <link href="../resource/css/style.css" rel="stylesheet">

	<script type="text/javascript">
         
            function Redirect() {
               window.location="home.php";
            }
            
            document.write("");
            setTimeout('Redirect()', 3000); 
         
      </script>
  <style>
      .gobtn{background-color:#952685;
      	color:#fff;
      	padding:10px 60px;
      	font-weight: bold;
		margin-right:60px;
      	}
  </style>
</head>
<body>
  <div id="wrapper">
  	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div style="min-height:600px;" class="col-md-6">
				<img style="margin-top:100px; border-radius: 10px;"  src="../resource/img/d.jpg" alt="Alumni banner" class=" img img-responsive">
				
				<a style="margin-top:20px; float:right;" href="home.php" class="btn btn-primary btn-lg gobtn">Go</a>
			</div>
			<div class="col-md-3"></div>	
		</div>
  	</div>
  </div>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <!-- javascript
    ================================================== -->
 <!-- Bootstrap core JavaScript -->
    <script src="../resource/jquery/jquery.min.js"></script>
    <script src="../resource/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>