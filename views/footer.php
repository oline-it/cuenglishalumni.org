
    <!-- Footer -->
    <footer class="py-5 cufooter">
      <div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="farea">
					<img src="../resource/img/logo2.png" alt="Logo" class="img img-responsive"/>
					<img src="../resource/img/flogo.png" alt="Logo" class="img img-responsive"/>
					<!--<p>graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful</p>-->
					<p class="srtad">
						Phone : 01714130077<br/>
						Email: reunion@cuenglishalumni.org
					</p>
				</div>
			</div>
			<div class="col-md-3 gtouch">
				<div class="farea">
					<h5>Get In Touch</h5>

					<form class="form-inline">
					  <div class="form-group">
						<div class="input-group">
						  <input type="email" class="form-control" id="email" placeholder="Enter your email">
							<div class="input-group-addon">
								<button type="submit" class="btn subbtn" name="submit">
									<i class="fa fa-paper-plane"></i>
								</button>
							</div>
						</div>
					  </div>
					</form>
					<div class="social">
						<a target="_blank" href="https://www.facebook.com/The-Chittagong-University-English-Alumni-Association-2548658381839264/"><img src="../resource/img/fb.png" alt="facebook" class="img img-responsive"/></a>
						<a target="_blank" href="#"><img src="../resource/img/ins.png" alt="instagram" class="img img-responsive"/></a>
					</div>
				</div>
			</div>
			<div class="col-md-3 link">
				<div class="farea">
					<h5>Useful Links</h5>
					<ul>
						<li><a href="#">Our Mission</a></li>
						<li><a href="#">Membership Policy</a></li>
						<li><a href="#">Scholarship & Donation</a></li>
						<li><a href="#">Life Members</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-2 link2">
				<div class="farea">
					<h5>Menus</h5>
					<ul>
						<li><a href="committee.php">Alumni Committee</a></li>
						<li><a href="registration.php">Registration</a></li>
						<li><a href="events.php">Events</a></li>
						<li><a href="contact.php">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
        <p class="m-0 text-center text-white">Copyright &copy; 2018 The Chittagong University English Alumni Association </p>
      </div>
     
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../resource/jquery/jquery.min.js"></script>
    <script src="../resource/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<!--make menu dropdown by js-->
	<script type="text/javascript">
		$('ul.navbar-nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
				}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
				  
	<!--countdown timer script-->
	<script src="../resource/js/jquery.countdown.min.js"></script>
	<script type="text/javascript">
		$('#countdown').countdown('2019/2/15', function(event) {
		// $(this).html(event.strftime('%w weeks %d days <br /> %H:%M:%S'));
		  $(this).html(event.strftime('<table class=""><tr><td>WEEK</td><td>DAY</td><td>HR</td><td>MIN</td><td>SEC</td><td></td></tr><tr><td><span class="day">%w</span></td><td><span class="day">%d</span></td><td><span class="hour">%H</span></td><td><span class="min">%M</span></td><td><span class="sec">%S</span></td><td>Remaining</td></tr></table>'));
		});
		
		$('#countdown2').countdown('2019/2/16', function(event) {
		// $(this).html(event.strftime('%w weeks %d days <br /> %H:%M:%S'));
		  $(this).html(event.strftime('<table class=""><tr><td>WEEK</td><td>DAY</td><td>HR</td><td>MIN</td><td>SEC</td><td></td></tr><tr><td><span class="day">%w</span></td><td><span class="day">%d</span></td><td><span class="hour">%H</span></td><td><span class="min">%M</span></td><td><span class="sec">%S</span></td><td>Remaining</td></tr></table>'));
		});
		
		
	</script>
  
	<!--rating member counter -->
	<script type="text/javascript">
		$('.counter').each(function() {
			  var $this = $(this),
				  countTo = $this.attr('data-count');
			  
			  $({ countNum: $this.text()}).animate({
				countNum: countTo
			  },

			  {

				duration: 8000,
				easing:'linear',
				step: function() {
				  $this.text(Math.floor(this.countNum));
				},
				complete: function() {
				  $this.text(this.countNum);
				  //alert('finished');
				}

			  });  
			  
			  

			});
	</script>
	<!--isotope image filtering supporting js -->
	<!--<script src="resource/js/isotope.pkgd.min.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function(){
			$(".filter-button").click(function(){
				var value = $(this).attr('data-filter');
				
				if(value == "all")
				{
					//$('.filter').removeClass('hidden');
					$('.filter').show('1000');
				}
				else
				{
		//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
		//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
					$(".filter").not('.'+value).hide('3000');
					$('.filter').filter('.'+value).show('3000');
					
				}
			});
			
			if ($(".filter-button").removeClass("active")) {
		$(this).removeClass("active");
		}
		$(this).addClass("active");
		});
	</script>
	<!--lightbox-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.js'></script>
	<script  src="../resource/js/index.js"></script>

	<!------------------ My Footer Script -------------->
	<script type="text/javascript">
		$('.dob-datepicker').datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: '-100y:c+nn',
			maxDate: '-1d'
		});
		</script>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script
		src="https://code.jquery.com/jquery-2.1.3.js"
		integrity="sha256-goy7ystDD5xbXSf+kwL4eV6zOPJCEBD1FBiCElIm+U8="
		crossorigin="anonymous"></script>

	<script>
		window.jQuery || document.write('<script src="../resource/js/jquery.min.js"><\/script>')
	</script>
	<script src="../resource/bootstrap/js/bootstrap.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../resource/js/ie10-viewport-bug-workaround.js"></script>
	<!-- Bootstrap Dropdown Hover JS -->
	<script src="../resource/bootstrap/js/jquery.js"> </script>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../resource/js/jquery.min.js"></script>
	<!--<script>window.jQuery || document.write('<script src="js/jquery.min.js"></script>')</script>-->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- Bootstrap Dropdown Hover JS -->
	<script src="../resource/js/bootstrap-dropdownhover.min.js"></script>


	<script type="text/javascript">
		/* Set the width of the side navigation to 250px */
		function openNav() {
			document.getElementById("mySidenav").style.width = "250px";
		}

		/* Set the width of the side navigation to 0 */
		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
		}
	</script>

	<script>
		jQuery(function($) {
			$('#message').fadeIn(500);
			$('#message').fadeOut (500);
			$('#message').fadeIn (500);
			$('#message').delay (2500);
			$('#message').fadeOut (2000);
		})

		$('#delete').on('click',function(){
			document.forms[1].action="deletemultiple.php";
			$('#multiple').submit();
		});

		//select all checkboxes
		$("#select_all").change(function(){  //"select all" change
			var status = this.checked; // "select all" checked status
			$('.checkbox').each(function(){ //iterate all listed checkbox items
				this.checked = status; //change ".checkbox" checked status
			});
		});

		$('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
			if(this.checked == false){ //if this item is unchecked
				$("#select_all")[0].checked = false; //change "select all" checked status to false
			}

//check "select all" if all checkbox items are checked
			if ($('.checkbox:checked').length == $('.checkbox').length ){
				$("#select_all")[0].checked = true; //change "select all" checked status to true
			}
		});


	</script>

	<!-- required for search, block 5 of 5 start -->
	<script>

		$(function() {
			var availableTags = [

				<?php
				echo $comma_separated_keywords;
				?>
			];
			// Filter function to search only from the beginning of the string
			$( "#searchID" ).autocomplete({
				source: function(request, response) {

					var results = $.ui.autocomplete.filter(availableTags, request.term);

					results = $.map(availableTags, function (tag) {
						if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
							return tag;
						}
					});

					response(results.slice(0, 15));

				}
			});
			$( "#searchID" ).autocomplete({
				select: function(event, ui) {
					$("#searchID").val(ui.item.label);
					$("#searchForm").submit();
				}
			});
		});

	</script>
	<!-- javascript
       ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../resource/bootstrap/js/jquery.js"></script>
	<script src="../resource/bootstrap/js/jquery-ui.js"></script>
	<script src="../resource/js/jquery.easing.1.3.js"></script>
	<script src="../resource/js/bootstrap.js"></script>
	<script src="../resource/js/bootstrap.min.js"></script>
	<script src="../resource/js/jcarousel/jquery.jcarousel.min.js"></script>
	<script src="../resource/js/jquery.fancybox.pack.js"></script>
	<script src="../resource/js/jquery.fancybox-media.js"></script>
	<script src="../resource/js/google-code-prettify/prettify.js"></script>
	<script src="../resource/js/portfolio/jquery.quicksand.js"></script>
	<script src="../resource/js/portfolio/setting.js"></script>
	<script src="../resource/js/jquery.flexslider.js"></script>
	<script src="../resource/js/jquery.nivo.slider.js"></script>
	<script src="../resource/js/modernizr.custom.js"></script>
	<script src="../resource/js/jquery.ba-cond.min.js"></script>
	<script src="../resource/js/jquery.slitslider.js"></script>
	<script src="../resource/js/animate.js"></script>

	<!-- Template Custom JavaScript File -->
	<script src="../resource/js/custom.js"></script>

  </body>
</html>