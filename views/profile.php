<?php
ob_start();
use Mpdf\Utils\UtfString;
include('header.php');
include('session.php');
include_once('printscript.php');

$objAlumni = new \App\Alumni\Alumni();

$objAlumni->setData($_SESSION);
if(isset($_GET)){
	$objAlumni->setData($_GET);
}

//$objAlumni->setData($_SESSION);
$objToArrayProfile=$objAlumni->view();

if($objToArrayProfile){   // $_SESSION['pdf']='pdf';
}
//var_dump($objToArrayProfile);
###################################Participant Count  #######################################3
$total_participant=1;

if($objToArrayProfile['spouse']>=1500){
	$spouse=1;
	$total_participant=$total_participant+$spouse;
}

if($objToArrayProfile['guest']>=1500){
	$total_guest=round($objToArrayProfile['guest']/1500);
	$total_participant=$total_participant+$total_guest;
}

if($objToArrayProfile['child']>=1000){
	$total_child=$objToArrayProfile['child']/1000;
	$total_participant=$total_participant+$total_child;
}
if($objToArrayProfile['infant']>=500){
	$total_infant=$objToArrayProfile['infant']/500;
	$total_participant=$total_participant+$total_infant;
}

if($objToArrayProfile['baby']==!null){
	$total_baby=1;
	$total_participant=$total_participant+$total_baby;
}
###################################Participant Count##############################

if($objToArrayProfile['membertype']==1){
	$membertype="<img width=\"150px\" src='img/lifemember.png'>";
} else{ $membertype=" ";}

if($objToArrayProfile['payment_status']=='Unpaid') {
	$payment="<a class='text-danger font-weight-bold'><b> Unpaid</b> </a>"; }
else{ $payment="<b class='text-primary '>Paid</b></a>";}

$photo=$objToArrayProfile['photo'];
$pid=$objToArrayProfile['lpad(id,4,0)'];
$full_name=$objToArrayProfile['full_name'];
$email=$objToArrayProfile['email'];
$mobile=$objToArrayProfile['mobile_no'];
$institute=$objToArrayProfile['institution'];
$batch=$objToArrayProfile['batch'];
$honors=$objToArrayProfile['honours'];
$ma=$objToArrayProfile['ma'];
$mphil=$objToArrayProfile['mphil'];
$phd=$objToArrayProfile['phd'];
$allParticiapnt=number_format($total_participant,0);

$html=<<<PDF
 <div   class="container ">
          <div align="center" class="row cardcenter">
              <div id="divprintview" style="width:595px; border:1px solid #CCCCCC;border-radius:5px;padding-left:30px; margin-bottom:10px;" class="divprintview profile col-md-6 col-md-offset-3">
                  <div class="text-center up cardcenter">
                      <h3 class="titA cardcenter">The Chittagong University English Alumni Association </h3>
                      <strong>UNIVERSITY OF CHITTAGONG</strong><br/>
                      <p class="cardcenter">4th English Alumni Reunion-2019<br/>
                     January 25-26, 2019</p>
                    </div>
                  <div align="center"  class="row table-responsive">
                      <table  class="table">
                          <thead>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><u><b>INVITATION CARD</b></u></td>
                          </tr>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><b>Participant's Information</b></td>
                          </tr>
                          <tr>
                              <td>
                                  <img src="uploads/$photo" width="80px"  alt="Applicant Name" class="img img-responsive">
                              </td>
                              <td></td>
                              <td style="text-align: center; padding-top: 20px;">$membertype</td>
                          </tr>
                          </thead>
                          <tr>
                            <td>Participant ID</td>
                            <td>:</td>
                            <td>$pid</td>
                          </tr>
                          <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td>$full_name</td>
                          </tr>
                          <tr>
                            <td>Department/Institute</td>
                            <td>:</td>
                            <td>$institute</td>
                          </tr>
                          <tr>
                            <td>Batch</td>
                            <td>:</td>
                            <td> $batch  <br>[H-$honors][M-$ma][MPil-$mphil][PHD-$phd]</td>
                          </tr>
                          <tr>
                              <td>Total Participants</td>
                              <td>:</td>
                              <td >$allParticiapnt</td>
                          </tr>
                          <tr>
                              <td>Payment Status:</td>
                              <td>:</td>
                              <td  >
                              $payment
                              </td>
                          </tr>
                      </table>
                  </div>
              </div>
          </div>
        </div>
PDF;
?>
<div class="container">
	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class=" col-md-4">
					<div class="inner-heading">

					</div>
				</div>
				<div class=" col-md-8">
					<ul style="background-color:inherit;" class="breadcrumb">
						<li><a href="home.php"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>

					</ul>
				</div>
			</div>
		</div>
	</section>
	<div id="dvContainer">
		<style>
			<?php
                    include ('../resource/css/printsetup.css');
            ?>
		</style>
		<section id="content">

			<?php
			echo $html;
			?>

		</section>
	</div>
	<div class="dpdf text-center">
		<button type="button" id="btnPrint"  class=" btn btn-dark">Print your card</button><br>
			<a href="profile.php?dpdf=<?php echo $_SESSION['pdf'];?>">Download As PDF</a>
	</div>


</div>
<?php

//if(isset($_GET['pdf'])){
include_once ('../vendor/autoload.php');
//$printsetup = file_get_contents('../resource/css/printsetup.css'); // external css
$bootstrap = file_get_contents('../resource/css/bootstrap.min.css'); // external css
//$css=$printsetup.$bootstrap;

$mpdf=new Mpdf\Mpdf();


$mpdf->WriteHTML($bootstrap,1);
$mpdf->WriteHTML($html,2);
//$md = UtfString::strcode2utf("&#1575;&#1610;&#1604;&#1575;&#1578; &#1601;&#1610;&#1605;&#1575;&#1575;&#1610;&#1604;&#1575;&#1578; &#1601;&#1610;&#1605;&#1575;");
//$mpdf->SetTitle($md);
// Output a PDF file directly to the browser

if(isset($_GET['dpdf'])=='pdf'){
	ob_clean();
	$mpdf->Output('card.pdf', 'D');
}

//}



if(isset($_GET['sendmail'])) {

$emailcontent=<<<EMAIL
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Email Confirmation:</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  /**
   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
   */
  @media screen {
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 400;
      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
    }
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 700;
      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
    }
  }
  /**
   * Avoid browser level font resizing.
   * 1. Windows Mobile
   * 2. iOS / OSX
   */
  body,
  table,
  td,
  a {
    -ms-text-size-adjust: 100%; /* 1 */
    -webkit-text-size-adjust: 100%; /* 2 */
  }
  /**
   * Remove extra space added to tables and cells in Outlook.
   */
  table,
  td {
    mso-table-rspace: 0pt;
    mso-table-lspace: 0pt;
  }
  /**
   * Better fluid images in Internet Explorer.
   */
  img {
    -ms-interpolation-mode: bicubic;
  }
  /**
   * Remove blue links for iOS devices.
   */
  a[x-apple-data-detectors] {
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    color: inherit !important;
    text-decoration: none !important;
  }
  /**
   * Fix centering issues in Android 4.4.
   */
  div[style*="margin: 16px 0;"] {
    margin: 0 !important;
  }
  body {
    width: 100% !important;
    height: 100% !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  /**
   * Collapse table borders to avoid space between cells.
   */
  table {
    border-collapse: collapse !important;
  }
  a {
    color: #1a82e2;
  }
  img {
    height: auto;
    line-height: 100%;
    text-decoration: none;
    border: 0;
    outline: none;
  }
  </style>

</head>
<body style="background-color: #e9ecef;">

  <!-- start preheader -->
  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
   Dear $full_name, Your registration request has been received.
  </div>
  <!-- end preheader -->

  <!-- start body -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="center" valign="top" style="padding: 36px 24px;">
              <a href="http://cuenglishalumni.org" target="_blank" style="display: inline-block;">
                <img src="../resource/img/logo.png" alt="Logo" border="0" width="" >
              </a>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
              <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Thank you for registering to Alumni !</h1>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p style="margin: 0;">
              Dear $full_name, <br>
              Your registration request has been received.  You loging credetials are as follows:.</p>
              <p>Email Address : $email</p>
              <p>Mobile : $mobile </p>
            </td>
          </tr>
          <!-- end copy -->

          <!-- start button -->
          <tr>
            <td align="left" bgcolor="#ffffff">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">
                          <a href="cuenglishalumni.org/views/profile.php?email=$email&mobile_no=$mobile&downloadcard=yes" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Download Invitation Card</a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- end button -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p>If you want to pusblish your creative writings on souvenir you are requested to send your writings to : <a style="color: red; font-weight: bold;">alumnisouvenir2019@gmail.com </a>  . </p>
              <p style="margin: 0;">We look forward to seeing you on 20 (Fri)-21(Sat) December 2019 . </p>

            </td>
          </tr>
          <!-- end copy -->


          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">Kind Regards, <br> President, The Chittagong University English Alumni Asociation <br> alumni@cuenglishalumni.org <br>
              <a style="text-decoration: none; color: rgb(0, 0, 0);" target="_blank" href="http://cuenglishalumni.org/">www.cuenglishalumni.org</a><br>
              <a href="https://www.facebook.com/pg/The-Chittagong-University-English-Alumni-Association-2548658381839264/posts/" style="color: rgb(0, 0, 0); text-decoration: none;">www.facebook.com</a> <br> 880 1714130077 </span><br>
            </td>
          </tr>
          <!-- end copy -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end copy block -->

    <!-- start footer -->
    <tr>
      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start permission -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end permission -->

          <!-- start unsubscribe -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end unsubscribe -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end footer -->

  </table>
  <!-- end body -->
</body>
</html>
EMAIL;





	require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->SMTPDebug  = 0;
	$mail->SMTPAuth   = true;
	$mail->SMTPSecure = "ssl";
	$mail->Host       = "mail.cuenglishalumni.org ";
	$mail->Port       = 465;
	$mail->AddAddress($email); // To
	$mail->Username="alumni@cuenglishalumni.org";
	$mail->Password="%Tu753951*#";
	$mail->SetFrom('alumni@cuenglishalumni.org','CU English Alumni');
	$mail->addCC('alumnisouvenir2019@gmail.com','CU English Alumni');
	$mail->addBCC('olineitbd@gmail.com','CU English Alumni');
	$mail->AddReplyTo("alumni@cuenglishalumni.org","CU English Alumni");
	$mail->Subject = "Alumni Reunion Registration Confirmation.";
	$message =$emailcontent;
	$mail->MsgHTML($message);
	$mail->Send();
	/*
	 * if($mail->Send()){
		Message::message("
<div class=\"alert alert-success\">
<strong>Email Sent!</strong> Please check your email for password reset link.
</div>");	}
	 * */

}
include('footer.php');

?>