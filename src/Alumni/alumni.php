<?php

namespace App\Alumni;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Alumni extends  DB{

private  $full_name, $membertype,$token, $photo,$dob, $blood,$institution,$designation,$present_address, $permanent_address,$batch,$id_no,$honours,$honours_year,$ma,$ma_year,$mphil,$mphil_year,$phd,$phd_year,$mobile_no,$email,$self,$spouse,$guest, $child,$infant,$baby,$lmfees,$total,$food,$Payment_mode,$payees_name,$transaction_no,$note,$donation;
private  $permitted_chars;
    public function setData($postData){

        if(array_key_exists('full_name',$postData)){
            $this->full_name = $postData['full_name'];
        } if(array_key_exists('membertype',$postData)){
            $this->membertype = $postData['membertype'];
        }if(array_key_exists('token',$postData)){
            $this->token = $postData['token'];
        }
        if(array_key_exists('photo',$postData)){
            $this->photo = $postData['photo'];
        }
        if(array_key_exists('dob',$postData)){
            $this->dob = $postData['dob'];
        } if(array_key_exists('blood',$postData)){
            $this->blood = $postData['blood'];
        }if(array_key_exists('institution',$postData)){
            $this->institution = $postData['institution'];
        }if(array_key_exists('designation',$postData)){
            $this->designation = $postData['designation'];
        }if(array_key_exists('present_address',$postData)){
            $this->present_address = $postData['present_address'];
        }if(array_key_exists('permanent_address',$postData)){
            $this->permanent_address = $postData['permanent_address'];
        }if(array_key_exists('batch',$postData)){
            $this->batch = $postData['batch'];
        }if(array_key_exists('id_no',$postData)){
            $this->id_no = $postData['id_no'];
        }if(array_key_exists('honours',$postData)){
            $this->honours = $postData['honours'];
        }if(array_key_exists('honours_year',$postData)){
            $this->honours_year = $postData['honours_year'];
        }if(array_key_exists('ma',$postData)){
            $this->ma = $postData['ma'];
        }if(array_key_exists('ma_year',$postData)){
            $this->ma_year = $postData['ma_year'];
        }if(array_key_exists('mphil',$postData)){
            $this->mphil = $postData['mphil'];
        }if(array_key_exists('mphil_year',$postData)){
            $this->mphil_year = $postData['mphil_year'];
        }if(array_key_exists('phd',$postData)){
            $this->phd = $postData['phd'];
        }if(array_key_exists('phd_year',$postData)){
            $this->phd_year = $postData['phd_year'];
        }if(array_key_exists('mobile_no',$postData)){
            $this->mobile_no = $postData['mobile_no'];
        }if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }if(array_key_exists('self',$postData)){
            $this->self = $postData['self'];
        }if(array_key_exists('spouse',$postData)){
            $this->spouse = $postData['spouse'];
        }if(array_key_exists('guest',$postData)){
            $this->guest = $postData['guest'];
        }if(array_key_exists('child',$postData)){
            $this->child = $postData['child'];
        }if(array_key_exists('infant',$postData)){
            $this->infant = $postData['infant'];
        }if(array_key_exists('baby',$postData)){
            $this->baby = $postData['baby'];
        }if(array_key_exists('lmfees',$postData)){
            $this->lmfees = $postData['lmfees'];}
        if(array_key_exists('total',$postData)){
            $this->total = $postData['total'];
        }if(array_key_exists('food',$postData)){
            $this->food = $postData['food'];
        }if(array_key_exists('Payment_mode',$postData)){
            $this->Payment_mode = $postData['Payment_mode'];
        }if(array_key_exists('payees_name',$postData)){
            $this->payees_name = $postData['payees_name'];
        }if(array_key_exists('transaction_no',$postData)){
            $this->transaction_no = $postData['transaction_no'];
        }if(array_key_exists('note',$postData)){
            $this->note = $postData['note'];
        }if(array_key_exists('donation',$postData)){
            $this->donation = $postData['donation'];
        }


            }
    public function objectToArray($objectData){

        $objectToArray = json_decode(json_encode($objectData), True);

        return $objectToArray;
    }

    public function generate_string($input, $strength = 16){

    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }
    return $random_string;
     }

    public function sms($mobileNumber,$message){

        ################ Getting SMS settings from DB ####################################
        $sql="SELECT * FROM settings WHERE name='sms'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $smsSettings=$STH->fetch();
        $objToArraySmsSet = json_decode(json_encode($smsSettings), True);
        ################ Getting SMS settings from DB Ended ###############################
        $user = $objToArraySmsSet['username'];
        $password = $objToArraySmsSet['password'];
        $sender = $objToArraySmsSet['senderid'];
        $url = "http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$message&GSM=$mobileNumber&";
        $smsResult = simplexml_load_file($url);
        return $smsResult;

    }

    public function store(){
        //var_dump($_POST);      die();

        $arrData = array($this->full_name,$this->membertype,$this->photo,$this->dob,$this->blood,$this->institution,$this->designation,$this->present_address,$this->permanent_address,$this->batch,$this->id_no,$this->honours_year,$this->ma_year,$this->mphil_year,$this->phd_year,$this->mobile_no,$this->email,$this->self,$this->spouse,$this->guest,$this->child,$this->infant,$this->baby,$this->lmfees,$this->total,$this->food,$this->Payment_mode,$this->payees_name,$this->transaction_no,$this->note,$this->token);
        $sql = "INSERT INTO cualumni(full_name,membertype, photo, dob, blood, institution, designation, present_address, permanent_address, batch, id_no, honours, ma, mphil, phd, mobile_no, email, self, spouse, guest, child, infant, baby,lmfees, total, food, Payment_mode, payees_name, transaction_no, note,token) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {

            if($_POST['donation']==!NULL){
                $sql="SELECT * FROM cualumni WHERE token='$this->token'";
                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                $lastRecord=$STH->fetch();
                $objToArrayLastRecord = json_decode(json_encode($lastRecord), True);

                $arrData = array($objToArrayLastRecord['id'],$this->donation,$this->note);
                $sql = "INSERT INTO doantion(dmid,amount, note) VALUES (?,?,?)";
                $STH = $this->DBH->prepare($sql);
                $STH->execute($arrData);
            }
            $to="88".$this->mobile_no; //Country code setup
            $gsm =$to;
            $msg ="Thank you for your registration! Username:$this->email and Password:".urlencode($this->mobile_no).". Login:www.cuenglishalumni.org";
            $sentSms=$this->sms($gsm,$msg);
            if($sentSms)
                $_SESSION['email']=$this->email;
               $_SESSION['mobile_no']=$this->mobile_no;
            Message::message("Success! You have  successfully registered:)");
       }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('profile.php?mobile_no='.$this->mobile_no.'&email='.$this->email.'&pdf=pdf&sendmail=ok');

    }


    public function view(){


        //lpad(idcolumn,4,'0')
        $sql='';

        if($_GET['id']=='all'){
            $sql = "SELECT * FROM cualumni";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();

        } else{
            $sql = "SELECT lpad(id,4,0),membertype, `full_name`, `photo`, `dob`, `blood`, `institution`, `designation`, `present_address`, `permanent_address`, `batch`, `id_no`, `honours`, `ma`, `mphil`, `phd`, `mobile_no`, `email`, `self`, `spouse`, `guest`, `child`, `infant`, `baby`, `total`, `food`, `Payment_mode`, `payees_name`, `transaction_no`, `note`,payment_status, `created`, `modified`, `softdeleted` FROM `cualumni`  WHERE mobile_no='$this->mobile_no' AND email='$this->email'";

            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $objToArray=$this->objectToArray($STH->fetch());
            if(!$objToArray){
                Message::message("Invalid Data");
                Utility::redirect('login.php');
            }
            return $objToArray;

        }


    }



}